import java.io.File;
import java.util.Date;

import org.jibble.pircbot.DccFileTransfer;
import org.jibble.pircbot.PircBot;
import org.jibble.pircbot.Queue;

import com.sun.corba.se.spi.servicecontext.SendingContextServiceContext;


public class QueueHandler extends Thread {
	
	private PircBot bot = null;
	private String botname = null;
	private String channel = null;
	
	private Queue lq = null;
	private int maxslots = 10;
	private volatile int freeslots = 10;
	private String delay = "NOW";
	private int queued = 0;
	private int queuesize = 999;
	private int speed = 0;
	private int songs = 0;
	private int totalsize = 0;
	private int servprio = 0;
	private Date listdate = null;
	private int uptime = 0;
	private String servinfo = "NiloServ 0.0.1";
	
	public QueueHandler() {
		lq = new Queue();
	}

	public void setBot(PircBot _bot, String name, String chan) {
		bot = _bot;
		botname = name;
		channel = chan;
	}
	
	public void listRequest(String nick) {
		freeslots--;
		DccFileTransfer dft = bot.dccSendFile(new File("c:\\ircbot\\Default_list.txt"), nick, 100000);
	}
	
	public void sendQueue(String nick) {
		System.out.println("Sending queue to: " + nick);
	}
	
	
	
	public void run() {
		while(true) {
			try {
				Thread.sleep(15000);
				bot.sendCTCPCommand(channel, "SLOTS " + freeslots + " " + maxslots + " " + delay + " " + queued + " " + queuesize + " " + speed + " " + songs + " " + totalsize + " " + servprio + " " + listdate + " " + uptime + " " + servinfo);
			} catch (Exception e) {
				
			}
			
		}
	}

	public void fileFinished(DccFileTransfer tran, Exception e) {
		freeslots++;
		if(e != null) e.printStackTrace();
	}

	public void sendFile(String sender, String message) {
		System.out.println("Sending: " + message + " to " + sender);
	}
	
}
