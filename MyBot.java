import org.jibble.pircbot.*;

public class MyBot extends PircBot {
    
	private QueueHandler qh = null;
	private String botname = null;
	private String channel = null;
	
    public MyBot(String name, String chan, QueueHandler _qh) {
    	botname = name;
    	channel = chan;
    	this.setName(name);
    	qh = _qh;
    }
    
    public void onFileTransferFinished(DccFileTransfer tran, Exception e) {
    	qh.fileFinished(tran, e);
    }
    
    public void onMessage(String channel, String sender, String login, String hostname, String message) {
    	if(message.equalsIgnoreCase("@" + botname)) {
    		qh.listRequest(sender);
    	}
    	if(message.equalsIgnoreCase("@" + botname + "-que")) {
    		qh.sendQueue(sender);
    	}
    	if(message.substring(0, botname.length() + 1).equalsIgnoreCase("!" +botname)) {
    		qh.sendFile(sender, message);
    	}
    	//System.out.println("Chan: " + channel + " sender: " + sender + " Login: " + login + " hostname: " + hostname + " message: " + message);
    }
    
}