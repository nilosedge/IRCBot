
public class IRCBot {

	    public static void main(String[] args) throws Exception {
	        
	        // Now start our bot up.
	    	String chan = "#test";
	    	String botname = "NiloBot";
	    	

	    	
	    	QueueHandler qh = new QueueHandler();
	    	MyBot bot = new MyBot(botname, chan, qh);
	    	qh.setBot(bot, botname, chan);
	    	qh.start();

	        
	        // Enable debugging output.
	        bot.setVerbose(true);
	        
	        // Connect to the IRC server.
	        bot.connect("irc.nilosplace.net");
	        //bot.connect("irc.phazenet.com");

	        // Join the #pircbot channel.
	        bot.joinChannel(chan);
	        //bot.joinChannel("#audiobooks");
	        
	    }
    
}
